package com.example.zf.travelsample.recommend.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.example.zf.travelsample.R;
import com.example.zf.travelsample.Utils;
import com.example.zf.travelsample.recommend.News;
import com.example.zf.travelsample.recommend.NewsAdapter;
import com.example.zf.travelsample.recommend.SpaceItemDecoration;

import org.litepal.LitePal;
import org.litepal.crud.LitePalSupport;

import java.util.ArrayList;
import java.util.List;

public class RecommendFragment extends Fragment {


    private RecyclerView recyclerView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recommend, container, false);
        initViews(view);
        return view;
    }

    private void initViews(View view) {
        recyclerView = view.findViewById(R.id.recycler_view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        List<News> newsList = LitePal.findAll(News.class);
        if (newsList.size() <= 0) {
            newsList = getList();
        }
        NewsAdapter newsAdapter = new NewsAdapter(newsList, getActivity());


        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new SpaceItemDecoration(Utils.dip2px(getActivity(), 8)));
        recyclerView.setAdapter(newsAdapter);
    }


    private List<News> getList() {
        List<News> newsList = new ArrayList();
        newsList.add(new News(1, "HotShop1", "instance 1.0km", false));
        newsList.add(new News(2, "HotShop2", "instance 1.2km", false));
        newsList.add(new News(3, "HotShop3", "instance 1.3km", false));
        newsList.add(new News(4, "HotShop4", "instance 1.4km", false));
        newsList.add(new News(5, "HotShop5", "instance 1.5km", false));
        newsList.add(new News(6, "HotShop6", "instance 1.6km", false));
        newsList.add(new News(7, "HotShop7", "instance 1.7km", false));
        newsList.add(new News(8, "HotShop8", "instance 2.0km", false));
        newsList.add(new News(9, "HotShop9", "instance 2.1km", false));
        newsList.add(new News(10, "HotShop10", "instance 2.2km", false));
        for (News news : newsList) {
            news.save();
        }
        return newsList;
    }
}
