package com.example.zf.travelsample.home.adapter;

public interface ItemRemove {

    void delete(int pos);
}
