package com.example.zf.travelsample.bill;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.zf.travelsample.R;
import com.example.zf.travelsample.view.MyListView;

import java.util.List;

public class BillAdapter extends ArrayAdapter<BillBean> {

    private List<BillBean> newsList;
    private Context context;

    public BillAdapter(@NonNull Context context, int resource, @NonNull List<BillBean> objects) {
        super(context, resource, objects);
        this.newsList = objects;
        this.context = context;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bill, parent, false);
        ViewHolder holder = new ViewHolder();

        holder.tvDay = convertView.findViewById(R.id.tv_day);
        holder.newsTitle = convertView.findViewById(R.id.tv_date);
        holder.listView = convertView.findViewById(R.id.my_listView);

        BillBean billBean = newsList.get(position);
        holder.tvDay.setText(billBean.day);
        holder.newsTitle.setText(billBean.yearAndMonth);
        holder.listView.setAdapter(new BillSubAdapter(context, 1, billBean.list, position));


        return convertView;
    }

    static class ViewHolder {

        TextView tvDay;
        TextView newsTitle;
        MyListView listView;
    }
}