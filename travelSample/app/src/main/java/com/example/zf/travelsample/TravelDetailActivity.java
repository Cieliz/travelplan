package com.example.zf.travelsample;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.example.zf.travelsample.bill.BillBean;
import com.example.zf.travelsample.bill.MessageEvent;
import com.example.zf.travelsample.home.TravelBean;
import com.example.zf.travelsample.home.TravellAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.litepal.LitePal;

import java.util.ArrayList;
import java.util.List;

public class TravelDetailActivity extends AppCompatActivity {


    private ListView listView;
    private TravellAdapter adapter;
    private List<TravelBean> list;

    private boolean is = true;

    private long pid;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBar();

        setContentView(R.layout.activity_travel);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getIntent().getStringExtra("title"));

        //Set the navigation icon after the setSupportActionBar method
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_institutions_delect);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        TextView tv = findViewById(R.id.tv_time);
        tv.setText(getIntent().getStringExtra("time"));

        pid = getIntent().getLongExtra("pid", 0);

        listView = findViewById(R.id.listView);
//        list = new ArrayList<>();
//        TravelBean bean1 = new TravelBean("6:00", "SFO-LAX", 1, 1);
//        TravelBean bean2 = new TravelBean("8:00", "LAX-JFX", 1, 0);
//        TravelBean bean3 = new TravelBean("10:00", "Package Manager ", 0, 1);
//        TravelBean bean4 = new TravelBean("12:00", "Open Window", 1, 1);
//        TravelBean bean5 = new TravelBean("14:00", "Play Billing", 1, 0);
//        TravelBean bean6 = new TravelBean("16:00", "Play Instant", 0, 1);
//        TravelBean bean7 = new TravelBean("18:00", "Play Text", 1, 0);
//        TravelBean bean8 = new TravelBean("20:00", "Install Test", 1, 1);
//        TravelBean bean9 = new TravelBean("22:00", "Play Instant", 1, 0);
//        TravelBean bean10 = new TravelBean("00:00", "Play Instant", 0, 1);
//        list.add(bean1);
//        list.add(bean2);
//        list.add(bean3);
//        list.add(bean4);
//        list.add(bean5);
//        list.add(bean6);
//        list.add(bean7);
//        list.add(bean8);
//        list.add(bean9);
//        list.add(bean10);

        list = init();
        adapter = new TravellAdapter(this, 1, list);
        listView.setAdapter(adapter);


        TextView tvAdd = findViewById(R.id.tv_add);
        tvAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TravelDetailActivity.this, AddEventActivity.class)
                        .putExtra("pid", pid));
            }
        });
    }

    private List<TravelBean> init() {
        List<TravelBean> tList = LitePal.findAll(TravelBean.class);
        List<TravelBean> tempList = new ArrayList<>();
        for (TravelBean bean : tList) {
            if (bean.pid == pid) {
                tempList.add(bean);
            }
        }
        return tempList;
    }

    @Override

    protected void onStart() {
        super.onStart();
        if (is) {
            EventBus.getDefault().register(this);
            is = false;
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        // EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {

        if (event.parentPos == -1) {
            list.clear();
            list.addAll(init());
            adapter.notifyDataSetChanged();
        } else {
            TravelBean bean = list.remove(event.parentPos);
            adapter.notifyDataSetChanged();
            LitePal.delete(TravelBean.class, bean.id);
        }
    }

    /**
     * Android 6.0 以上设置状态栏颜色
     */
    protected void setStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            // Sets the status bar background color
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().setStatusBarColor(Color.WHITE);

            // If the color is bright, set the status bar text to black
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        }

    }

}
