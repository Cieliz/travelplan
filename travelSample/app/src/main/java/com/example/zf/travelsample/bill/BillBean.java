package com.example.zf.travelsample.bill;

import com.example.zf.travelsample.bill.ui.BillSubBean;

import org.litepal.crud.LitePalSupport;

import java.util.ArrayList;
import java.util.List;

public class BillBean extends LitePalSupport {

    public long id;
    public String day;
    public String yearAndMonth;
    public String date;
    public List<BillSubBean> list = new ArrayList<>();
}
