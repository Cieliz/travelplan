package com.example.zf.travelsample.home.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.zf.travelsample.R;

import java.util.List;

public class PersonAdapter extends ArrayAdapter<Person>{

    private int resourceId;
    private Context context;
    /* overrides the constructor, which is the implementation of the underlined part above */
    public PersonAdapter(Context context, int textViewResourceId, List<Person> objects){
        super(context, textViewResourceId, objects);
        resourceId = textViewResourceId;
        this.context = context;
    }     /* Overrides the getView() method, which is called when each child is scrolled into the screen */
    public View getView(int position, View convertView, final ViewGroup parent){
        final Person person = getItem(position); //获取当前项的Person实例
        View view;
        ViewHolder viewHolder;  //The inner class defined below is used to hold instances of image, name, delete, etc., instead of getting the control instance by findViewById every time a slide loads
        /* The converView parameter in the getView() method represents the previously loaded layout */        if(convertView == null){            /* If the converView parameter value is null, the LayoutInflater is used to load the layout */            view = LayoutInflater.from(getContext()).inflate(resourceId, parent, false);
            viewHolder = new ViewHolder();
            /* Call the findViewById() method of the View to get the image and name instances, respectively */
            viewHolder.image = (ImageView) view.findViewById(R.id.image);
            viewHolder.name = (TextView) view.findViewById(R.id.name);
            viewHolder.delete = view.findViewById(R.id.delete_button);
            view.setTag(viewHolder);
        }
        else{            /* Otherwise, reuse the converView directly */
            view = convertView;
            viewHolder = (ViewHolder) view.getTag();
        }        /* The setImageResource() and setText() methods are called to set the image and text to be displayed */
        viewHolder.image.setImageResource(person.getImageId());
        viewHolder.name.setText(person.getName());
        /* Event listening response section, that is, click delete icon and avatar will display the reminder information respectively */
        viewHolder.delete = view.findViewById(R.id.delete_button);
        viewHolder.delete.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Toast.makeText(getContext(), "you clicked delete button", Toast.LENGTH_SHORT).show();
            }
        });
        viewHolder.image.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Toast.makeText(getContext(), "you clicked image", Toast.LENGTH_SHORT).show();
            }
        });
        return view; //return layout
    }
    class ViewHolder{
        ImageView image;
        TextView name;
        View delete;
    }
}