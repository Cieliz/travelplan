package com.example.zf.travelsample.bill.ui;

import androidx.annotation.NonNull;

import org.litepal.crud.LitePalSupport;

public class BillSubBean extends LitePalSupport {

    public long id;
    public String sort;
    public String bz;
    public String time;
    public String money;
    public String date;

    @NonNull
    @Override
    public String toString() {
        return "{sort=" + sort + ",bz=" + bz + ",time=" + time + ",money=" + money + "date=" + date +
                "}";
    }
}
