package com.example.zf.travelsample.home.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.zf.travelsample.R;
import com.example.zf.travelsample.TravelDetailActivity;
import com.example.zf.travelsample.home.ui.HomeBean;

import java.util.List;

public class MyAdapter extends BaseAdapter {
    private Context content;
    private List<HomeBean> datas;

    public MyAdapter(Context context, List<HomeBean> datas) {
        this.content = context;
        this.datas = datas;
    }

    private ItemRemove itemRemove;

    public void setItemRemove(ItemRemove remove) {
        itemRemove = remove;
    }

    @Override
    public int getCount() {
        return datas.size();
    }

    @Override
    public Object getItem(int position) {
        return datas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder = new ViewHolder();
        convertView = LayoutInflater.from(content).inflate(R.layout.item_home_test, null);

        viewHolder.tvAddress = convertView.findViewById(R.id.tv_address);
        viewHolder.tvDate = convertView.findViewById(R.id.tv_time);
        viewHolder.tvDelete = convertView.findViewById(R.id.tv_delete);
        viewHolder.rl = convertView.findViewById(R.id.content);


        final HomeBean bean = datas.get(position);
        viewHolder.tvAddress.setText(bean.address);
        viewHolder.tvDate.setText(bean.date);

        viewHolder.tvDelete.setTag(position);

        viewHolder.tvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (itemRemove != null) {
                    itemRemove.delete((Integer) viewHolder.tvDelete.getTag());
                }
            }
        });

        viewHolder.rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                content.startActivity(new Intent(content, TravelDetailActivity.class)
                        .putExtra("title", viewHolder.tvAddress.getText().toString())
                        .putExtra("time", viewHolder.tvDate.getText().toString())
                        .putExtra("pid", bean.id));
            }
        });

        return convertView;
    }

    class ViewHolder {
        TextView tvAddress;
        TextView tvDate;
        TextView tvDelete;
        RelativeLayout rl;
    }

}


