package com.example.zf.travelsample.recommend;

import org.litepal.annotation.Column;
import org.litepal.crud.LitePalSupport;


public class News extends LitePalSupport {

    @Column(nullable = false)
    public long id;
    public String title;
    public String instance;
    public boolean isCollect;

    public News(long id, String title, String instance, boolean isCollect) {
        this.title = title;
        this.instance = instance;
        this.id = id;
        this.isCollect = isCollect;
    }

}