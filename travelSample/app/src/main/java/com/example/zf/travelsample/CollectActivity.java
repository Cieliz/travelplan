package com.example.zf.travelsample;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.zf.travelsample.home.ui.HomeBean;
import com.example.zf.travelsample.recommend.News;

import org.litepal.LitePal;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class CollectActivity extends AppCompatActivity {

    private ListView listView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_collect);


        Toolbar toolbar = findViewById(R.id.toolbar);

        //Set the navigation icon after the setSupportActionBar method
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.icon_head);


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        listView = findViewById(R.id.listView);
        List<News> list = LitePal.findAll(News.class);
        List<News> newList = new ArrayList<>();
        if (list.size() > 0) {
            for (News news : list) {
                if (news.isCollect) {
                    newList.add(news);
                }
            }
        }
        CollectAdapter adapter = new CollectAdapter(this, 1, newList);
        listView.setAdapter(adapter);

    }

}