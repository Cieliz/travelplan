package com.example.zf.travelsample.recommend;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.zf.travelsample.R;

import org.litepal.LitePal;

import java.util.List;
import java.util.Random;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.ViewHolder> {

    private List<News> newsList;
    private Context context;

    public NewsAdapter(List<News> newsList, Context context) {
        this.newsList = newsList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.news_item, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {
        final News news = newsList.get(i);

        ViewGroup.LayoutParams params = viewHolder.newsImage.getLayoutParams();
        params.height = params.height + new Random().nextInt(300);
        viewHolder.newsImage.setLayoutParams(params);

        viewHolder.newsTitle.setText(news.title);
        viewHolder.tvInstance.setText(news.instance);
        if (news.isCollect) {
            viewHolder.tvCollect.setText("CancelCollect");
        } else {
            viewHolder.tvCollect.setText("Collect");
        }
        viewHolder.tvCollect.setTag(String.valueOf(news.isCollect));
        viewHolder.tvCollect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean state = Boolean.valueOf(viewHolder.tvCollect.getTag().toString());
                if (state) {
                    viewHolder.tvCollect.setText("Collect");
                } else {
                    viewHolder.tvCollect.setText("CancelCollect");
                }
                viewHolder.tvCollect.setTag(String.valueOf(!state));
                News albumToUpdate = LitePal.find(News.class, news.id);
                albumToUpdate.isCollect = !state; // raise the price
                albumToUpdate.save();
            }
        });
    }

    @Override
    public int getItemCount() {
        return newsList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView newsImage;
        TextView newsTitle;
        View v;
        TextView tvCollect;
        TextView tvInstance;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            newsImage = itemView.findViewById(R.id.newsPic);
            newsTitle = itemView.findViewById(R.id.newsTitle);
            tvCollect = itemView.findViewById(R.id.tv_collect);
            tvInstance = itemView.findViewById(R.id.tv_instance);

        }
    }
}