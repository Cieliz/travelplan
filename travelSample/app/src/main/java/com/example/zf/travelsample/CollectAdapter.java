package com.example.zf.travelsample;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.zf.travelsample.bill.BillBean;
import com.example.zf.travelsample.bill.BillSubAdapter;
import com.example.zf.travelsample.recommend.News;
import com.example.zf.travelsample.view.MyListView;

import java.util.List;

public class CollectAdapter extends ArrayAdapter<News> {

    private List<News> newsList;
    private Context context;
    private int width;

    public CollectAdapter(@NonNull Context context, int resource, @NonNull List<News> objects) {
        super(context, resource, objects);
        this.newsList = objects;
        this.context = context;

        width = (Utils.getScreenWidth(context) - Utils.dip2px(context, 40)) / 2;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_collect, parent, false);
        ViewHolder holder = new ViewHolder();

        holder.iv = convertView.findViewById(R.id.newsPic);
        holder.tvTitle = convertView.findViewById(R.id.newsTitle);
        holder.tvDistance = convertView.findViewById(R.id.tv_instance);

        News billBean = newsList.get(position);
        holder.tvTitle.setText(billBean.title);
        holder.tvDistance.setText(billBean.instance);

        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) holder.iv.getLayoutParams();
        lp.width = width;
        holder.iv.setLayoutParams(lp);


        return convertView;
    }

    static class ViewHolder {

        TextView tvTitle;
        TextView tvDistance;
        ImageView iv;
    }
}