package com.example.zf.travelsample;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.zf.travelsample.bill.BillBean;
import com.example.zf.travelsample.bill.MessageEvent;
import com.example.zf.travelsample.bill.ui.BillSubBean;

import org.greenrobot.eventbus.EventBus;
import org.litepal.LitePal;

import java.util.Calendar;
import java.util.List;

public class RecordActivity extends AppCompatActivity {

    private static final String TAG = "RecordActivity";

    private LinearLayout tvStart;
    private TextView tvSort;
    private EditText etBz;
    private EditText etMoney;
    private LinearLayout lyTime;
    private TextView tvYear;
    private LinearLayout tvData;
    private TextView tvTime;

    private String myear;
    private String mday;


    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_record);


        Toolbar toolbar = findViewById(R.id.toolbar);

        //Set the navigation icon after the setSupportActionBar method
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.icon_head);

        Button button = findViewById(R.id.btn_start);


        tvStart = findViewById(R.id.tv_start);
        tvSort = findViewById(R.id.tv_end);
        etBz = findViewById(R.id.et_bz);
        etMoney = findViewById(R.id.et_address);
        tvData = findViewById(R.id.ly_date);
        tvYear = findViewById(R.id.tv_year);
        lyTime = findViewById(R.id.ly_time);
        tvTime = findViewById(R.id.tv_time);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        tvStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Record", "click");
                AlertDialog.Builder builder = new AlertDialog.Builder(RecordActivity.this);
                builder.setTitle("Expense classification");//expense classification
                final String[] items = new String[]{"clothes", "food", "house", "traffic"};//four money categories
                builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        tvSort.setText(items[which]);
                        dialog.dismiss();
                    }
                });
                builder.show();
            }
        });


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String money = etMoney.getText().toString();
                if (TextUtils.isEmpty(money)) {
                    Utils.showToast(RecordActivity.this, "please input pay money");
                    return;
                }
                Log.d(TAG, "money=" + money);
                String fenlei = tvSort.getText().toString();
                if (TextUtils.isEmpty(fenlei)) {
                    Utils.showToast(RecordActivity.this, "please input pay sort");
                    return;
                }
                Log.d(TAG, "sort=" + fenlei);
                String date = tvYear.getText().toString();
                if (TextUtils.isEmpty(date)) {
                    Utils.showToast(RecordActivity.this, "please input date");
                    return;
                }
                Log.d(TAG, "date=" + date);
                String time = tvTime.getText().toString();
                if (TextUtils.isEmpty(time)) {
                    Utils.showToast(RecordActivity.this, "please input time");
                    return;
                }
                Log.d(TAG, "time=" + time);
                String bz = etBz.getText().toString();
                if (TextUtils.isEmpty(bz)) {
                    Utils.showToast(RecordActivity.this, "please input remarks");
                    return;
                }
                Log.d(TAG, "bz=" + bz);

                BillSubBean bean = new BillSubBean();
                bean.bz = bz;
                bean.id = System.currentTimeMillis();
                bean.money = money;
                bean.date = date;
                bean.sort = fenlei;
                bean.time = time + "  cash";//money
                bean.save();
                Log.d(TAG, "BillSubBean=" + bean.toString());

                List<BillBean> list = LitePal.findAll(BillBean.class);
                Log.d(TAG, "size=" + list.size());

                boolean isSameDay = false;
                if (list.size() > 0) {
                    for (BillBean b : list) {
                        if (b.date.equals(date)) {
                            b.list.add(bean);
                            isSameDay = true;
                            Log.d(TAG, "Added on the same day");//Added today
                            break;
                        }
                    }
                }

                if (!isSameDay) {
                    Log.d(TAG, "new day");//new day
                    BillBean billBean = new BillBean();
                    billBean.day = mday;
                    billBean.yearAndMonth = myear;
                    billBean.date = date;
                    billBean.id = System.currentTimeMillis();
                    billBean.list.add(bean);
                    billBean.save();
                }


                EventBus.getDefault().post(new MessageEvent(-1, 1));

                finish();
            }
        });

        tvData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDataPick();
            }
        });

        lyTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTime();
            }
        });

    }

    private void showDataPick() {


        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                tvYear.setText(year + "." + (month + 1) + "." + dayOfMonth);
                myear = (month + 1) + "." + year;
                mday = dayOfMonth + "日";
            }
        }, year, month, day).show();
    }

    private void showTime() {
        Calendar calendar = Calendar.getInstance();
        TimePickerDialog dialog = new TimePickerDialog(RecordActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                tvTime.setText(hourOfDay + ":" + minute);
            }
        },
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE),
                true);
        dialog.show();
    }
}