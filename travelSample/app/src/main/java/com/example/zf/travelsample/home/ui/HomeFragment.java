package com.example.zf.travelsample.home.ui;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.media.Image;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.zf.travelsample.AddPlanActivity;
import com.example.zf.travelsample.R;
import com.example.zf.travelsample.TravelDetailActivity;
import com.example.zf.travelsample.home.adapter.ItemRemove;
import com.example.zf.travelsample.home.adapter.MyAdapter;

import org.litepal.LitePal;

import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;

public class HomeFragment extends Fragment implements View.OnClickListener, ItemRemove {

    private TextView mTvUpcoming;
    private TextView mTvPast;
    private LinearLayout mLyContainer;
    private ListView mListView;
    private ImageView ivAdd;


    private int mCurrentSelect;
    private int mIndex = 0;
    private int mWidth;

    private TextView mTvCombines[];

    private List<HomeBean> mList;
    private MyAdapter mAdapter;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_home, container, false);

        initViews(view);

        return view;
    }

    private void initViews(View view) {
        mTvUpcoming = view.findViewById(R.id.tv_upcoming);
        mTvPast = view.findViewById(R.id.tv_past);
        mLyContainer = view.findViewById(R.id.ly_container);
        mListView = view.findViewById(R.id.list_view);

        mTvUpcoming.setOnClickListener(this);
        mTvPast.setOnClickListener(this);

        View v = view.findViewById(R.id.v_status);

        Resources resources = getResources();
        int resourceId = resources.getIdentifier("status_bar_height", "dimen", "android");
        int height = resources.getDimensionPixelSize(resourceId);

        if (height > 0) {
            LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) v.getLayoutParams();
            lp.height = height;
            v.setLayoutParams(lp);
        }

        view.findViewById(R.id.iv_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getActivity(), AddPlanActivity.class), 1);
            }
        });

    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        mTvPast.setText("Past");
        mTvUpcoming.setText("Upcoming");

        mTvCombines = new TextView[]{mTvUpcoming, mTvPast};

        WindowManager manager = (WindowManager) getActivity()
                .getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics outMetrics = new DisplayMetrics();
        manager.getDefaultDisplay().getMetrics(outMetrics);
        mWidth = outMetrics.widthPixels;

        int width = mWidth / 4;

        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) mLyContainer.getLayoutParams();
        lp.leftMargin = width;
        lp.rightMargin = width;
        mLyContainer.setLayoutParams(lp);

        mTvUpcoming.setBackgroundColor(Color.parseColor("#169EFF"));
        mTvUpcoming.setTextColor(Color.parseColor("#000000"));
        mTvPast.setBackgroundColor(Color.parseColor("#3C4460"));
        mTvPast.setTextColor(Color.parseColor("#2A6FAA"));

        mList = new ArrayList<>();
//        HomeBean bean1 = new HomeBean("New York Trip", "2020.5.1-2020.5.2");
//        HomeBean bean2 = new HomeBean("European", "2020.5.3-2020.5.4");
//        HomeBean bean3 = new HomeBean("China", "2020.5.9-2020.5.10");
//        HomeBean bean4 = new HomeBean("Japan", "2020.5.11-12");
//        mList.add(bean1);
//        mList.add(bean2);
//        mList.add(bean3);
//        mList.add(bean4);
        List<HomeBean> l = LitePal.findAll(HomeBean.class);
        Log.d("HomeFragment", "localDate_size=" + l.size());
        mList.addAll(l);
        mAdapter = new MyAdapter(getActivity(), mList);
        mAdapter.setItemRemove(this);
        mListView.setAdapter(mAdapter);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && RESULT_OK == resultCode) {
            HomeBean bean = (HomeBean) data.getSerializableExtra("bean");
            mList.add(bean);
            mAdapter.notifyDataSetChanged();
            bean.save();
        }
    }

    private void setSelectState() {
        if (mCurrentSelect != mIndex) {
            mTvCombines[mIndex].setBackgroundColor(Color.parseColor("#169EFF"));
            mTvCombines[mIndex].setTextColor(Color.parseColor("#000000"));
            mTvCombines[mCurrentSelect].setBackgroundColor(Color.parseColor("#3C4460"));
            mTvCombines[mCurrentSelect].setTextColor(Color.parseColor("#2A6FAA"));
            mCurrentSelect = mIndex;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_upcoming:
                mIndex = 0;
                setSelectState();
                break;
            case R.id.tv_past:
                mIndex = 1;
                setSelectState();
                break;
            default:
                break;

        }
    }

    @Override
    public void delete(int pos) {
        HomeBean bean = mList.remove(pos);
        LitePal.delete(HomeBean.class, bean.id);
        mAdapter.notifyDataSetChanged();
    }

}
