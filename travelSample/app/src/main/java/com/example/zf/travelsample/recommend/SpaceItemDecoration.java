package com.example.zf.travelsample.recommend;

import android.graphics.Rect;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

public class SpaceItemDecoration extends RecyclerView.ItemDecoration {
    private int space;

    public SpaceItemDecoration(int space) {
        this.space = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        //Each cell has a left and bottom spacing except for the first cell
        //outRect.left = space;
        outRect.bottom = space;
        //Since there are only 3 rows in each row, the first one is a multiple of 3, so let's set the left margin to 0
//        if (parent.getChildLayoutPosition(view) % 2 != 0) {
//            outRect.left = space;
//        }
    }

}
