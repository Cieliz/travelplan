package com.example.zf.travelsample.home;

import org.litepal.crud.LitePalSupport;

public class TravelBean extends LitePalSupport {

    public long id;
    public String time;
    public String name;
    public int leftState;
    public int rightState;
    public long pid;
}
