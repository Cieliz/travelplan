package com.example.zf.travelsample.home.adapter;


public class MyContent {
    private String content;

    public MyContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
