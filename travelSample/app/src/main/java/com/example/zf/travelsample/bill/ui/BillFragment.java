package com.example.zf.travelsample.bill.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.LongDef;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.zf.travelsample.R;
import com.example.zf.travelsample.RecordActivity;
import com.example.zf.travelsample.RecordEvent;
import com.example.zf.travelsample.bill.BillAdapter;
import com.example.zf.travelsample.bill.BillBean;
import com.example.zf.travelsample.bill.MessageEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.litepal.LitePal;

import java.util.ArrayList;
import java.util.List;

public class BillFragment extends Fragment {

    private ListView listView;
    private BillAdapter adapter;
    List<BillBean> list;

    private TextView tvRecord;
    private TextView tvRemain;

    private boolean is = true;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bill, container, false);
        initViews(view);
        return view;
    }

    private void initViews(View view) {
        listView = view.findViewById(R.id.listView);
        tvRemain = view.findViewById(R.id.tv_remain);
        tvRecord = view.findViewById(R.id.tv_jl);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


//        List<String> list1 = new ArrayList<>();
//        list1.add("测试1");
//        list1.add("测试2");
//        list1.add("测试3");
//        BillBean bean1 = new BillBean("21日", "2.2020  周二", list1);
//
//        List<String> list2 = new ArrayList<>();
//        list2.add("花费1");
//        list2.add("花费2");
//        BillBean bean2 = new BillBean("19日", "1.2020  周三", list2);
//
//        List<String> list3 = new ArrayList<>();
//        list3.add("购物1");
//        BillBean bean3 = new BillBean("16日", "9.2019  周五", list3);
//
//        list.add(bean1);
//        list.add(bean2);
//        list.add(bean3);

        list = init();

        adapter = new BillAdapter(getActivity(), 1, list);
        listView.setAdapter(adapter);


        tvRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), RecordActivity.class));
            }
        });

    }


    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        Log.d("BillFragment", "hidden=" + hidden);
//        if(!hidden){
//            EventBus.getDefault().register(this);
//        }else{
//            EventBus.getDefault().unregister(this);
//        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (is) {
            EventBus.getDefault().register(this);
            is = false;
        }
        Log.d("BillFragment", "onStart");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d("BillFragment", "onStop");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("BillFragment", "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("BillFragment", "onPause");
    }

    private List<BillBean> init() {
        long total = 0;
        List<BillBean> list1 = LitePal.findAll(BillBean.class);
        Log.d("BillFragment", "list_size=" + list1.size());
        if (list1.size() > 0) {
            List<BillSubBean> subList = LitePal.findAll(BillSubBean.class);
            Log.d("BillFragment", "subList_size=" + subList.size());
            if (subList.size() > 0) {
                for (BillBean billBean : list1) {
                    Log.d("BillFragment", "billBean_date=" + billBean.date);
                    List<BillSubBean> tempList = new ArrayList<>();
                    for (BillSubBean billSubBean : subList) {
                        Log.d("BillFragment", "billSubBean.date=" + billSubBean.date);
                        if (billBean.date.equals(billSubBean.date)) {
                            tempList.add(billSubBean);
                            total += Long.valueOf(billSubBean.money);
                        }
                    }
                    billBean.list.addAll(tempList);
                }
            }
        }
        tvRemain.setText(String.valueOf(total));
        return list1;
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        Log.d("BillFragment", "parentPos=" + event.parentPos + ",childPos=" + event.childPos);

        if (event.parentPos != -1) {
            BillBean bean = list.get(event.parentPos);
            BillSubBean subBean = bean.list.remove(event.childPos);
            LitePal.delete(BillSubBean.class, subBean.id);
            long t = Long.valueOf(tvRemain.getText().toString());
            t -= Long.valueOf(subBean.money);
            tvRemain.setText(String.valueOf(t));
            if (bean.list.isEmpty()) {
                list.remove(event.parentPos);
                LitePal.delete(BillBean.class, bean.id);
            }
        } else {
            list.clear();
            list.addAll(init());
        }
        adapter.notifyDataSetChanged();
    }
}
