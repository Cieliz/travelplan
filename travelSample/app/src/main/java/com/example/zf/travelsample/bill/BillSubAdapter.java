package com.example.zf.travelsample.bill;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.zf.travelsample.R;
import com.example.zf.travelsample.bill.ui.BillSubBean;

import org.greenrobot.eventbus.EventBus;

import java.util.List;
import java.util.concurrent.TimeoutException;

public class BillSubAdapter extends ArrayAdapter<BillSubBean> {

    private List<BillSubBean> newsList;
    private int pos;

    public BillSubAdapter(@NonNull Context context, int resource, @NonNull List<BillSubBean> objects, int pos) {
        super(context, resource, objects);
        this.newsList = objects;
        this.pos = pos;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sub_bill, parent, false);
        final ViewHolder holder = new ViewHolder();

        holder.tvSort = convertView.findViewById(R.id.tv_sort);
        holder.tvBz = convertView.findViewById(R.id.tv_bz);
        holder.tvTime = convertView.findViewById(R.id.tv_time);
        holder.tvPay = convertView.findViewById(R.id.tv_money);
        holder.tvDelete = convertView.findViewById(R.id.tv_delete);
        holder.tvDelete.setTag(String.valueOf(position));

        holder.tvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new MessageEvent(pos, Integer.parseInt(holder.tvDelete.getTag().toString())));
            }
        });

        BillSubBean bean = newsList.get(position);
        holder.tvSort.setText(bean.sort);
        holder.tvBz.setText(bean.bz);
        holder.tvTime.setText(bean.time);
        holder.tvPay.setText((bean.money));

        return convertView;
    }

    static class ViewHolder {

        TextView tvSort;
        TextView tvBz;
        TextView tvTime;
        TextView tvPay;
        TextView tvDelete;
    }
}