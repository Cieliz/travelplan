package com.example.zf.travelsample;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.zf.travelsample.home.ui.HomeBean;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class AddPlanActivity extends AppCompatActivity {

    private TextView tvStart;
    private TextView tvEnd;
    private CheckBox checkBox;
    private EditText et;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_add);


        Toolbar toolbar = findViewById(R.id.toolbar);

        //Set the navigation icon after the setSupportActionBar method
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.icon_head);

        Button button = findViewById(R.id.btn_start);
        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) button.getLayoutParams();
        lp.width = Utils.getScreenWidth(this) / 2;
        lp.leftMargin = Utils.getScreenWidth(this) / 4;
        button.setLayoutParams(lp);

        tvStart = findViewById(R.id.tv_start);
        tvEnd = findViewById(R.id.tv_end);
        checkBox = findViewById(R.id.cb);
        et = findViewById(R.id.et_address);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isCheck = checkBox.isChecked();
                Log.d("AddPlanActivity", isCheck + "");
                String address = et.getText().toString();
                if (TextUtils.isEmpty(address)) {
                    Utils.showToast(AddPlanActivity.this, "please input destination");
                    return;
                }
                String start, end;
                if (!isCheck) {
                    start = tvStart.getText().toString();
                    if (TextUtils.isEmpty(start)) {
                        Utils.showToast(AddPlanActivity.this, "please input start date");
                        return;
                    }
                    end = tvEnd.getText().toString();
                    if (TextUtils.isEmpty(start)) {
                        Utils.showToast(AddPlanActivity.this, "please input end date");
                        return;
                    }

                    DateFormat df= new SimpleDateFormat("yyyy.MM.dd");
                    try {
                        Date date1=df.parse(start);
                        Date date2=df.parse(end);
                        if(date2.before(date1)){
                            Utils.showToast(AddPlanActivity.this, "Start date should be less than end date");
                            return;
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

//                    if (start.compareTo(end) >= 0) {
//                        Utils.showToast(AddPlanActivity.this, "Start date should be less than end date");
//                        return;
//                    }
                } else {
                    Calendar c = Calendar.getInstance();
                    start = c.get(Calendar.YEAR) + "." + (c.get(Calendar.MONTH) + 1) + "." + c.get(Calendar.DAY_OF_MONTH);
                    c.add(Calendar.DAY_OF_MONTH, 1);
                    end = c.get(Calendar.YEAR) + "." + (c.get(Calendar.MONTH) + 1) + "." + c.get(Calendar.DAY_OF_MONTH);

                }

                Intent intent = new Intent();
                HomeBean bean = new HomeBean(System.currentTimeMillis(), address, start + "-" + end);
                intent.putExtra("bean", bean);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        tvStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDataPick(tvStart);
            }
        });

        tvEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDataPick(tvEnd);
            }
        });
    }

    private void showDataPick(final TextView tv) {


        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                tv.setText(year + "." + (month + 1) + "." + dayOfMonth);
            }
        }, year, month, day).show();
    }
}