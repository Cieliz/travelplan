package com.example.zf.travelsample.center.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.zf.travelsample.CollectActivity;
import com.example.zf.travelsample.CollectAdapter;
import com.example.zf.travelsample.R;

public class CenterFragment extends Fragment {


    private Button mBtnLogin;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_center, container, false);
        initViews(view);
        return view;
    }

    private void initViews(View view) {

        WindowManager manager = (WindowManager) getActivity()
                .getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics outMetrics = new DisplayMetrics();
        manager.getDefaultDisplay().getMetrics(outMetrics);
        int width = outMetrics.widthPixels;

        mBtnLogin = view.findViewById(R.id.btn_login);
        mBtnLogin.setText("Login");
        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) mBtnLogin.getLayoutParams();
        lp.width = width / 2 - 64;
        mBtnLogin.setLayoutParams(lp);

        TextView tvOrder = view.findViewById(R.id.tv_order);
        tvOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), CollectActivity.class));
            }
        });
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
