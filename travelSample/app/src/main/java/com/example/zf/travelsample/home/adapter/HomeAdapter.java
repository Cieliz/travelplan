package com.example.zf.travelsample.home.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.zf.travelsample.R;
import com.example.zf.travelsample.home.ui.HomeBean;

import java.util.List;

public class HomeAdapter extends ArrayAdapter<HomeBean> {

    private int mWidth;
    private Context mCtx;


    public HomeAdapter(@NonNull Context context, int resource, @NonNull List<HomeBean> objects, int width) {
        super(context, resource, objects);
        this.mWidth = width;
        mCtx = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {


        View view;
        ViewHolder viewHolder;

        if (convertView == null) {
            view = LayoutInflater.from(mCtx).inflate(R.layout.item_home, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.iv = view.findViewById(R.id.iv);
            viewHolder.tvAddress = view.findViewById(R.id.tv_address);
            viewHolder.tvDate = view.findViewById(R.id.tv_time);
            viewHolder.tvDelete = view.findViewById(R.id.tv_delete);
            viewHolder.llDelete = view.findViewById(R.id.ly_delete);
            view.setTag(viewHolder);
        } else {
            view = convertView;
            viewHolder = (ViewHolder) view.getTag();
        }

        final HomeBean homeBean = getItem(position);
        viewHolder.tvAddress.setText(homeBean.address);
        viewHolder.tvDate.setText(homeBean.date);
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) viewHolder.iv.getLayoutParams();
        lp.width = mWidth;
        lp.height = mWidth;
        viewHolder.iv.setLayoutParams(lp);

        FrameLayout.LayoutParams llp = (FrameLayout.LayoutParams) viewHolder.llDelete.getLayoutParams();
        llp.height = mWidth;
        viewHolder.llDelete.setLayoutParams(llp);


        viewHolder.tvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mCtx, "delete", Toast.LENGTH_SHORT).show();
            }
        });

        return view;
    }

    class ViewHolder {
        TextView tvAddress;
        TextView tvDate;
        ImageView iv;
        TextView tvDelete;
        LinearLayout llDelete;
    }
}
