package com.example.zf.travelsample;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.zf.travelsample.bill.MessageEvent;
import com.example.zf.travelsample.home.TravelBean;
import com.example.zf.travelsample.home.ui.HomeBean;

import org.greenrobot.eventbus.EventBus;

import java.util.Calendar;

public class AddEventActivity extends AppCompatActivity {

    private Button tvStart;
    private EditText et;

    private long pid;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_add_event);


        Toolbar toolbar = findViewById(R.id.toolbar);

        //Set the navigation icon after the setSupportActionBar method
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.icon_head);

        tvStart = findViewById(R.id.btn_sure);
        et = findViewById(R.id.et_event);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        pid = getIntent().getLongExtra("pid", 0);

        tvStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String address = et.getText().toString();
                if (TextUtils.isEmpty(address)) {
                    Utils.showToast(AddEventActivity.this, "please input event");
                    return;
                }
                TravelBean bean = new TravelBean();
                bean.name = address;
                bean.id = System.currentTimeMillis();
                bean.pid = pid;
                bean.leftState = 0;
                bean.rightState = 0;
                bean.save();

                finish();
                EventBus.getDefault().post(new MessageEvent(-1, 0));

            }
        });

    }

}