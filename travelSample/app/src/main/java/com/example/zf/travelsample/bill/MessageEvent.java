package com.example.zf.travelsample.bill;

public class MessageEvent {

    public int parentPos;
    public int childPos;

    public MessageEvent(int pos, int childPos) {
        this.parentPos = pos;
        this.childPos = childPos;
    }
}
