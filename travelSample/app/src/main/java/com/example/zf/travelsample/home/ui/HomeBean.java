package com.example.zf.travelsample.home.ui;

import org.litepal.annotation.Column;
import org.litepal.crud.LitePalSupport;

import java.io.Serializable;

public class HomeBean extends LitePalSupport implements Serializable {


    public HomeBean(long id, String location, String time) {
        this.address = location;
        this.date = time;
        this.id = id;
    }

    @Column(nullable = false)
    public long id;
    public String address;
    public String date;
}
