package com.example.zf.travelsample;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.example.zf.travelsample.bill.ui.BillFragment;
import com.example.zf.travelsample.center.ui.CenterFragment;
import com.example.zf.travelsample.home.ui.HomeFragment;
import com.example.zf.travelsample.recommend.ui.RecommendFragment;

public class MainActivity extends AppCompatActivity {


    private Button[] mBtns;
    private Fragment[] mFragments;

    private int mIndex = 0;
    private int mCurrentTabIndex = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            window.setStatusBarColor(Color.TRANSPARENT);
        }

        setContentView(R.layout.activity_main);


        initBottomBtns();

        initContentFragments();
    }

    private void initBottomBtns() {
        mBtns = new Button[4];
        mBtns[0] = findViewById(R.id.btn_main_home);
        mBtns[1] = findViewById(R.id.btn_main_bill);
        mBtns[2] = findViewById(R.id.btn_main_recommend);
        mBtns[3] = findViewById(R.id.btn_main_center);
        mBtns[0].setSelected(true);
    }

    private void initContentFragments() {
        mFragments = new Fragment[]{new HomeFragment(), new BillFragment(),
                new RecommendFragment(), new CenterFragment()};

        getSupportFragmentManager().beginTransaction()
                .add(R.id.fr_content, mFragments[0])
                .show(mFragments[0])
                .commit();
    }

    public void onTabClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_main_home:
                mIndex = 0;
                break;
            case R.id.btn_main_bill:
                mIndex = 1;
                break;
            case R.id.btn_main_recommend:
                mIndex = 2;
                break;
            case R.id.btn_main_center:
                mIndex = 3;
                break;
            default:
                break;
        }
        if (mCurrentTabIndex != mIndex) {
            FragmentTransaction trx = getSupportFragmentManager().beginTransaction();
            trx.hide(mFragments[mCurrentTabIndex]);
            if (!mFragments[mIndex].isAdded()) {
                Log.d("MainActivity", "not add");
                trx.add(R.id.fr_content, mFragments[mIndex]);
            } else {
                Log.d("MainActivity", "is add");
            }
            trx.show(mFragments[mIndex]).commitAllowingStateLoss();

            mBtns[mCurrentTabIndex].setSelected(false);
            // Set the current TAB to selected status
            mBtns[mIndex].setSelected(true);
            mCurrentTabIndex = mIndex;
        }
    }
}
