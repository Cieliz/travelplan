package com.example.zf.travelsample.home;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.zf.travelsample.R;
import com.example.zf.travelsample.bill.BillBean;
import com.example.zf.travelsample.bill.BillSubAdapter;
import com.example.zf.travelsample.bill.MessageEvent;
import com.example.zf.travelsample.view.MyListView;

import org.greenrobot.eventbus.EventBus;

import java.util.List;
import java.util.concurrent.TimeoutException;

public class TravellAdapter extends ArrayAdapter<TravelBean> {

    private List<TravelBean> newsList;
    private Context context;

    public TravellAdapter(@NonNull Context context, int resource, @NonNull List<TravelBean> objects) {
        super(context, resource, objects);
        this.newsList = objects;
        this.context = context;
    }


    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        if (newsList.get(position).rightState == 1) {
            return 1;
        } else {
            return 0;
        }
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if (getItemViewType(position) == 1) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_travel, parent, false);
        } else {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_travelx, parent, false);
        }

        final ViewHolder holder = new ViewHolder();


        holder.vFirst = convertView.findViewById(R.id.v_first);
        holder.tvLeft = convertView.findViewById(R.id.tv_left);
        holder.tvDelete = convertView.findViewById(R.id.tv_delete);

        holder.tvDelete.setTag(String.valueOf(position));

        TravelBean billBean = newsList.get(position);

        if (getItemViewType(position) == 1) {
            holder.tvTime = convertView.findViewById(R.id.time);
            holder.tvName = convertView.findViewById(R.id.tv_name);
            holder.tvRight = convertView.findViewById(R.id.tv_right);


            holder.tvTime.setText(billBean.time);
            holder.tvName.setText(billBean.name);

            if (billBean.leftState == 1) {
                holder.tvTime.setVisibility(View.VISIBLE);
                holder.tvLeft.setVisibility(View.VISIBLE);
                holder.tvRight.setVisibility(View.VISIBLE);
            } else {
                holder.tvTime.setVisibility(View.INVISIBLE);
                holder.tvLeft.setVisibility(View.INVISIBLE);
                holder.tvRight.setVisibility(View.INVISIBLE);
            }
        } else {
            holder.tv1 = convertView.findViewById(R.id.tv1);
            holder.tv1.setText(billBean.name);
        }
        if (position == 0) {
            holder.vFirst.setVisibility(View.INVISIBLE);
        } else {
            holder.vFirst.setVisibility(View.VISIBLE);
        }


        holder.tvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new MessageEvent(Integer.valueOf(holder.tvDelete.getTag().toString()), 0));
            }
        });

        return convertView;
    }

    static class ViewHolder {

        TextView tvTime;
        TextView tvName;
        TextView tvLeft;
        TextView tvRight;
        View vFirst;
        TextView tvDelete;
        TextView tv1;
    }
}